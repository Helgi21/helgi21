## Helgi Steinarr Júlíusson


- 🌱 I’m currently learning CS @ Reykjavík University.
- 🔭 I’m currently experimenting with integrating a modern JavaScript pipeline into a Django application to increase my knowledge of modern webapps.
- 💬 Ask me about anything tech related!

- ⚡ Fun fact: Although my main focus for the last ~5years has been computer science related, my main hobby is getting my hands dirty working on cars and also scuba diving!

- 📫 How to reach me: helgi203(at)gmail.com


---
    | Python | JavaScript | C# | C++ | Java |
---
    | Django | React, React-Native | ElectronJS | BootStrap |
    | NodeJS |
---
    | HTML, CSS3, SCSS | Unity3D | Git | Linux |
    
